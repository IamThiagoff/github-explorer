import { RepositoryItem } from "./ResositoryItem";
import '../styles/repositories.scss';
import { useState, useEffect } from "react";

interface Repository {
    name: string,
    description: string,
    html_url: string
}

export function RepositoryList() {
    const [repositories, setRespositories] = useState<Repository[]>([]);

    useEffect(() => {
        fetch('https://api.github.com/orgs/rocketseat/repos')
        .then(response => response.json())
        .then(date => setRespositories(date))
    }, []);

    return(
        <section className="repository-list">
            <h1>Lista de Repositorios</h1>

            <ul>
                {repositories.map(repository => {
                    return <RepositoryItem key={repository.name} repository={repository} name={""} description={""} html_url={""} />
                })}
                
            </ul>
        </section>
    )
}
